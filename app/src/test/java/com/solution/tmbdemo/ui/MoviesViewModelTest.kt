package com.solution.tmbdemo.ui

import android.util.Log
import com.solution.tmbdemo.network.MoviesRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockedStatic
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MoviesViewModelTest {

    @Mock
    lateinit var repository:MoviesRepository
    var log: MockedStatic<Log> = mockStatic(Log::class.java)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @Test
    fun getMovies() {
        //given
        var model = MoviesModel(repository)
        Mockito.`when`(repository.checkNetworkConnectivity()).thenReturn(true)

        // when
        runBlocking {
            launch(Dispatchers.Main) {
                var log: MockedStatic<Log> = mockStatic(Log::class.java)
                model.getMovies()
            }
        }

        //Mockito.verify(model).schools
        assertEquals(0, model.movie.value?.size?:0)
        runBlocking {
            delay(999)
            verify(repository,times(1)).getMovies(anyString());
        }
    }
}