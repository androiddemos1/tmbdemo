package com.solution.tmbdemo.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.solution.tmbdemo.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}