package com.solution.tmbdemo.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.solution.tmbdemo.*
import com.solution.tmbdemo.data.Movie
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.movies_list_fragment.*

@AndroidEntryPoint
class MoviesFragment : Fragment(),RecyclerViewClickListener {

    private val viewModel: MoviesModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movies_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         viewModel.getMovies()

        viewModel.movies.observe(viewLifecycleOwner, { movies -> recycler_view_movieslist.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.adapter = MovieAdapter(movies,this)
                 it.addItemDecoration(DividerItemDecoration(context,LinearLayoutManager.VERTICAL))

        }
        })
    }

    override fun onListItemClick(view: View, movie:Movie) {
        val bundle = Bundle()
        bundle.putString("title",movie.title)
        bundle.putString("overview",movie.overview)
        bundle.putString("genre",movie.genre_ids.toString())

        Navigation.findNavController(view).navigate(R.id.action_moviesFragment_to_movie_fragment,bundle);
    }

}