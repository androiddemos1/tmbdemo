package com.solution.tmbdemo.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TMBApplication: Application()