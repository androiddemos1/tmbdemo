package com.solution.tmbdemo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.solution.tmbdemo.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.movie_fragment.*
import kotlinx.android.synthetic.main.movie_fragment.view.*

@AndroidEntryPoint
class MovieFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var title:String = arguments?.get("title") as String
        var overview:String = arguments?.get("overview") as String
        var genre:String = arguments?.get("genre") as String
        rLayoutDetails.textViewTitle.text = title
        rLayoutDetails.textViewOverview.text = overview
        rLayoutDetails.textViewGenre.text = genre

    }

}