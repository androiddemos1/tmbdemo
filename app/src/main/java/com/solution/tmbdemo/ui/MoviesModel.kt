package com.solution.tmbdemo.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.solution.tmbdemo.data.Movie
import com.solution.tmbdemo.util.Coroutines
import com.solution.tmbdemo.network.MoviesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import javax.inject.Inject

@HiltViewModel
class MoviesModel @Inject constructor(
     private val repository: MoviesRepository
      ) : ViewModel() {

    private lateinit var job: Job
    private lateinit var page:Number

    companion object{
        const val TAG = "MoviesViewModel"
    }
    private val _movies = MutableLiveData<List<Movie>>()
    val movies:LiveData<List<Movie>>
    get() = _movies

    fun getMovies() {
        Log.d(TAG,"ENTER -> getMoviesList")
        if(repository.checkNetworkConnectivity()) {
            page = 1
            job = Coroutines.ioThenMain(
                {
                    repository.getMovies(
                        page.toString()
                    )
                },
                {
                    _movies.value = it?.results
                }
            )
        }
        Log.d(TAG,"EXIT -> getMoviesList")
    }

    override fun onCleared() {
        super.onCleared()
        if(::job.isInitialized) job.cancel()
    }


}