package com.solution.tmbdemo.ui

import android.view.View
import com.solution.tmbdemo.data.Movie

interface RecyclerViewClickListener {
    fun onListItemClick(view: View, movie: Movie)
}