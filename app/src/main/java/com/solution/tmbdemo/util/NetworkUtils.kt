package com.solution.tmbdemo.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

object NetworkUtils {

     fun checkForInternet(context: Context): Boolean {

        val c_manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            val network =  c_manager.activeNetwork ?: return false
            val active_network = c_manager.getNetworkCapabilities(network) ?: return false
            when {
                active_network.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                active_network.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                else ->false
            }
        }else{
            val network =  c_manager.activeNetworkInfo ?: return false
            network.isConnected
        }

    }
}