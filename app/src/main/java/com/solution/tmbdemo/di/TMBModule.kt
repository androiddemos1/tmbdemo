package com.solution.tmbdemo.di

import com.solution.tmbdemo.network.MoviesApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object TMBModule {

    @Provides
    fun provideMovieApi():MoviesApi{
        return MoviesApi.invoke()
    }


}