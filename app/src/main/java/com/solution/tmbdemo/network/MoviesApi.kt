package com.solution.tmbdemo.network

import com.solution.tmbdemo.BuildConfig
import com.solution.tmbdemo.data.Movie
import com.solution.tmbdemo.data.Movies
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface MoviesApi {
   @GET("3/movie/popular?api_key=023a906eea8ae294c4543e0d04c620f1")
   suspend fun getMovies(@Query("\$page") page: String,): Response<Movies>

    companion object{
        operator fun invoke(): MoviesApi {
            return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.API_URL).build().create(MoviesApi::class.java)
        }


    }
}