package com.solution.tmbdemo.network

import android.content.Context
import android.widget.Toast
import com.solution.tmbdemo.util.NetworkUtils
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

open class MoviesRepository @Inject constructor(@ApplicationContext context: Context,
                                                private val api: MoviesApi
): SafeApiRequest() {
    private var mContext : Context = context
    fun checkNetworkConnectivity():Boolean{
        if(NetworkUtils.checkForInternet(mContext)) {
            return true
        }else{
            Toast.makeText(mContext,"No Network Connectivity", Toast.LENGTH_LONG).show()
            return false
        }
    }
    suspend fun getMovies(page:String) = apiRequest {
        api.getMovies(page)
    }

}